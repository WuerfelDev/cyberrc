#include <Servo.h>

enum class Switch {
  DOWN, CENTER, UP
};
enum class Blinkers {
  LEFT, RIGHT, BOTH, NONE
};
enum class Mode {
  OFF, MANUAL, AUTOMATIC, FAILSAFE
};

Servo steering;
Servo speedcontroller;
Blinkers blinkers = Blinkers::NONE;
Mode mode = Mode::OFF;


// PWM, but limited to 8 leds (pin shortage)
int led_front[] = {11, 12, 13}; //TODO 8 leds
// PWM, dimmed when on, full bright when braking
int led_brake[] = {2, 3, 4}; //TODO 5 leds
// No PWM, no need to dimm blinkers
int led_blinker_left[] = {23};
int led_blinker_right[] = {22};

/*
  for (byte i = 0; i < (sizeof(ledx) / sizeof(int)); i=i+1) {
    // Do something with ledx[i]
  }
*/

int steering_pin = 28;
int speedcontroller_pin = 32;



/* Pins on Arduino Mega:
   2: led brake
   3: led brake
   4: led brake
   5: led brake
   6: led brake
   9: led front lower
  10: led front lower
  11: led front lower
  12: led front lower
  13: led front lower
  18: Receiver
  19: Receiver
  28: steering servo
  32: speedcontroller
  44: led front top
  45: led front top
  46: led front top
  53: led failsafe status
*/



// if x is outside the expected bounds, it returns the extremum within the bounds
int betterMap(int x, int fromLow, int fromHigh, int toLow, int toHigh) {
  return (x <= fromLow) ? toLow : (x >= fromHigh) ? toHigh : map(x, fromLow, fromHigh, toLow, toHigh);
}



Switch getSwitchState(int val) {
  if (val < 667) {
    return Switch::DOWN;
  } else if (val > 1333) {
    return Switch::UP;
  } else {
    return Switch::CENTER;
  }
}


void setBlinker(Blinkers b) {
  if (blinkers != b) {
    // Reset before entering new mode
    for (byte i = 0; i < (sizeof(led_blinker_right) / sizeof(int)); i = i + 1) {
      digitalWrite(led_blinker_right[i], LOW);
    }
    for (byte i = 0; i < (sizeof(led_blinker_left) / sizeof(int)); i = i + 1) {
      digitalWrite(led_blinker_left[i], LOW);
    }
    blinkers = b;
  }
}


Mode getMode(int channel, bool failsafe) {
  if (failsafe) return Mode::FAILSAFE;
  switch (getSwitchState(channel)) {
    case Switch::DOWN: return Mode::OFF;
    case Switch::CENTER: return Mode::MANUAL;
    case Switch::UP: return Mode::AUTOMATIC;
  }
}



void setup() {

  // Initialize + Blink all leds
  for (byte i = 0; i < (sizeof(led_front) / sizeof(int)); i = i + 1) {
    pinMode(led_front[i], OUTPUT);
    digitalWrite(led_front[i], HIGH);
  }
  for (byte i = 0; i < (sizeof(led_brake) / sizeof(int)); i = i + 1) {
    pinMode(led_brake[i], OUTPUT);
    digitalWrite(led_brake[i], HIGH);
  }
  for (byte i = 0; i < (sizeof(led_blinker_right) / sizeof(int)); i = i + 1) {
    pinMode(led_blinker_right[i], OUTPUT);
    digitalWrite(led_blinker_right[i], HIGH);
  }
  for (byte i = 0; i < (sizeof(led_blinker_left) / sizeof(int)); i = i + 1) {
    pinMode(led_blinker_left[i], OUTPUT);
    digitalWrite(led_blinker_left[i], HIGH);
  }
  delay(1000);
  for (byte i = 0; i < (sizeof(led_front) / sizeof(int)); i = i + 1) {
    digitalWrite(led_front[i], LOW);
  }
  for (byte i = 0; i < (sizeof(led_brake) / sizeof(int)); i = i + 1) {
    digitalWrite(led_brake[i], LOW);
  }
  for (byte i = 0; i < (sizeof(led_blinker_right) / sizeof(int)); i = i + 1) {
    digitalWrite(led_blinker_right[i], LOW);
  }
  for (byte i = 0; i < (sizeof(led_blinker_left) / sizeof(int)); i = i + 1) {
    digitalWrite(led_blinker_left[i], LOW);
  }


  // For debugging
  Serial.begin(115200);
  pinMode(53, OUTPUT); // Failsafe status led


  // Custom baud rate for SBUS protocol
  Serial1.begin(100000, SERIAL_8E2);
}




void loop() {
  // static is faster than global
  static int    channels[18];
  static byte   buffer[25];
  static int    idx; // counter to fill buffer
  static int    errors = 0;
  static int    lost = 0;


  if (Serial1.available()) {
    byte b = Serial1.read();
    if (idx != 0 || b == 15) {
      buffer[idx++] = b;
    }
    if (idx == 25) {
      idx = 0;
      if (buffer[24] != 0x00) {
        errors++;
      } else {
        // magic from https://github.com/robotmaker/Arduino_SBUS
        channels[1] = ((buffer[1] | buffer[2] << 8) & 0x07FF);
        channels[2] = ((buffer[2] >> 3 | buffer[3] << 5) & 0x07FF);
        channels[3] = ((buffer[3] >> 6 | buffer[4] << 2 | buffer[5] << 10) & 0x07FF);
        channels[4] = ((buffer[5] >> 1 | buffer[6] << 7) & 0x07FF);
        channels[5] = ((buffer[6] >> 4 | buffer[7] << 4) & 0x07FF);
        channels[6] = ((buffer[7] >> 7 | buffer[8] << 1 | buffer[9] << 9) & 0x07FF);
        channels[7] = ((buffer[9] >> 2 | buffer[10] << 6) & 0x07FF);
        channels[8] = ((buffer[10] >> 5 | buffer[11] << 3) & 0x07FF);
        channels[9] = ((buffer[12] | buffer[13] << 8) & 0x07FF);
        channels[10] = ((buffer[13] >> 3 | buffer[14] << 5) & 0x07FF);
        channels[11] = ((buffer[14] >> 6 | buffer[15] << 2 | buffer[16] << 10) & 0x07FF);
        channels[12] = ((buffer[16] >> 1 | buffer[17] << 7) & 0x07FF);
        channels[13] = ((buffer[17] >> 4 | buffer[18] << 4) & 0x07FF);
        channels[14] = ((buffer[18] >> 7 | buffer[19] << 1 | buffer[20] << 9) & 0x07FF);
        channels[15] = ((buffer[20] >> 2 | buffer[21] << 6) & 0x07FF);
        channels[16] = ((buffer[21] >> 5 | buffer[22] << 3) & 0x07FF);
        channels[17] = ((buffer[23]) & 0x0001) ? 2047 : 0;
        channels[18] = ((buffer[23] >> 1) & 0x0001) ? 2047 : 0;


        bool failsafe = ((buffer[23] >> 3) & 0x0001) ? 1 : 0;

        if ((buffer[23] >> 2) & 0x0001) lost++;



        //  once mode changes
        Mode current_mode = getMode(channels[5], failsafe);
        if (mode != current_mode) {
          mode = current_mode;

          // Reset values
          setBlinker(Blinkers::NONE);
          digitalWrite(53, LOW);

          switch (mode) {
            case Mode::FAILSAFE:
              setBlinker(Blinkers::BOTH);
              digitalWrite(53, HIGH);
            case Mode::OFF:
              if (steering.attached()) steering.detach();
              if (speedcontroller.attached()) speedcontroller.detach();
              break;
            case Mode::MANUAL:
            case Mode::AUTOMATIC:
              if (!steering.attached()) steering.attach(steering_pin);
              if (!speedcontroller.attached()) speedcontroller.attach(speedcontroller_pin);
          }
        }


        
        if (mode == Mode::MANUAL) {
          // Microseconds instead of angles for higher accuracy
          int steeeer = betterMap(channels[2], 180, 1800, 2000, 1000);
          if(steering.readMicroseconds() != steeeer) steering.writeMicroseconds(steeeer);

          bool reverse = getSwitchState(channels[7])==Switch::UP;
          int speeeed = 1500 + (2 * reverse - 1 ) * betterMap(channels[1], 200, 1800, 0, 500);
          if(speedcontroller.readMicroseconds() != speeeed) speedcontroller.writeMicroseconds(speeeed);
          
        } else if (mode == Mode::AUTOMATIC) {
          // self driving
        }

        

        // Automatic lights
        if (mode == Mode::MANUAL || mode == Mode::AUTOMATIC) {
          int a = steering.readMicroseconds(); // we dont have to worry about the input to a mode
          if (abs(1500-a) > 150) { // avoid blinking near center
            Blinkers b = (a > 1500) ? Blinkers::LEFT : Blinkers::RIGHT;
            setBlinker(b);
          } else {
            setBlinker(Blinkers::NONE);
          }
        }


        // Car lights brightness
        if (mode != Mode::FAILSAFE) {
          int brightness = betterMap(channels[10], 200, 1800, 0, 255);
          for (byte i = 0; i < (sizeof(led_front) / sizeof(int)); i = i + 1) {
            analogWrite(led_front[i], brightness);
          }
          for (byte i = 0; i < (sizeof(led_brake) / sizeof(int)); i = i + 1) {
            analogWrite(led_brake[i], brightness > 190 ? 190 : brightness);
          }
        }


      }
    }
  }
  // Blinking as set in var blinkers
  if (blinkers != Blinkers::NONE) {
    bool value = millis() / 500 % 2;
    if (blinkers == Blinkers::RIGHT or blinkers == Blinkers::BOTH) {
      for (byte i = 0; i < (sizeof(led_blinker_right) / sizeof(int)); i = i + 1) {
        digitalWrite(led_blinker_right[i], value);
      }
    }
    if (blinkers == Blinkers::LEFT or blinkers == Blinkers::BOTH) {
      for (byte i = 0; i < (sizeof(led_blinker_left) / sizeof(int)); i = i + 1) {
        digitalWrite(led_blinker_left[i], value);
      }
    }
  }
}
