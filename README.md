# CyberRC

Building a fresh RC car from an old HBX Bonzer / Cross Tiger

- Chassis: stock (Tamiya [TL-01B](https://www.tamiya.com/english/rc/rcmanual/tl01b_baja.pdf))
- Steering servo: stock
- Motor: stock (540 probably?)
- Speed controller: [Reely brushed speed control](https://www.conrad.com/p/reely-model-car-brushed-speed-control-load-max-60-a-motor-limit-turns-20-2250414)
- Receiver: [Jumper R1](https://www.jumper-rc.com/products/receivers/r1/)
- Brain: Arduino Mega
- Car body: Custom. From cardboard(?) to match the Cybertruck design

## Checklist

- [X] Receive control events
- [X] Ability to control the car manually
- [ ] Calibration. Maybe using sliders (How to save it?)
- [X] Implement failsafe mode
- [X] Detect switches of transmitter - set modes
- [ ] Automated driving (very basic)
- [X] Add blinkers and light
- [ ] Smart light (blinkers + brake)
- [ ] Build car body

## Dataflow diagram

> Add in the future: lights, sensors(?), other arduino, ...

![](imgs/dataflow.png)

## OpenTX settings

Channel | Function | Input | Description
-------|-----|----|---
1 | Throttle | Left up/down (Thr)
2 | Steering | Right left/right (Ail)
3 | tbd | Right up/down (Ele)
4 | tbd | Left left/right (Rud)
5 | mode | left 3switch (SA) | park,manual,self driving
6 | tbd | right 3switch (SB)
7 | reverse | left 2switch (SC)
8 | tbd | right 2switch (SD)
9 | tbd | left slider (S1)
10 | lights | right slider (S2) | off+brightness of lights

