# Notes

## Steering

The wheels turn easier/smoother to the right and 'bumpy' to the left\
Also there is a tiny time difference:
- right to left: ~250ms
- left to right: ~350ms
but it could just be a measurement error (me with the stopwatch lol)

### Servo positions

- ~~center 95~~
- ~~right 60~~
- ~~left 125~~

center 90, each direction 30

### Steering angle

- centered. difference by ~2° (or that might be the camera lens)
- inner wheel max turning 27°/28° 
- outer wheel max turning 20.5° 

differences likely caused by loosly installed tires.

## Ideas

- 3d print custom rear body mounts for an uninterupted truckbed: https://gitlab.com/DavidDiPaola/cad_rc \
either a low mount that attaches in the bed, or wide appart
- Build body from light cardboard, supported by small wood beams on the edges - also better to mount leds
- body inspiration on a rc car body ratio: https://www.thingiverse.com/thing:4047300
- print prototype + body shape: https://www.caranddriver.com/features/a31931603/build-your-own-paper-tesla-cybertruck/
- fade leds without using PWM: https://www.robotshop.com/community/forum/t/fading-an-led-with-hardware/8724
- light bar could be: 
 - edge lit acrylic
 - leds in silicon
 - plastic that reflects light like in keyboard
 - leds in resin (front light can actually shine, but the sides light the rest of the bar)


## Attention!
- switches are detected as lower position if transmitter is off. (value 0 instead of 150(?) )
 


## sbus

- min ah 172
- max at 1811
- no tranceiver 0

gimbals may not reach these extrema
